from socket import *
import sys, cPickle, pprint
#--------------------------------------------------------
class SocketClosed(Exception) : pass
#LOG = [ 0, sys.stderr ] #loglevel, logfile
#--------------------------------------------------------
def send_string( connect, string ) :
    connect.send( '%08i'%len(string) )
    connect.send( string )
    '''if LOG[0] : print>>LOG[1],'SEND %s BYTES TO %s'%( len(string),
        connect.getpeername() )
    if LOG[0] == 2 : print>>LOG[1], string
    if LOG[0] : LOG[1].flush()'''
def recv_string( connect ) :
 '''try : length = int( connect.recv(8) )
 except : raise SocketClosed()'''
 length = int( connect.recv(8) )
 string = connect.recv( length )
 while len(string) < length : string += connect.recv( length -
len(string) )
 '''if LOG[0] : print>>LOG[1],'RECV %s BYTES FROM %s'%( length,
connect.getpeername() )
 if LOG[0] == 2 : print>>LOG[1], string
 if LOG[0] : LOG[1].flush()'''
 return string
 #--------------------------------------------------------
def send( connect, data ) :
 if data.__class__ == str : send_string( connect, 'S'+data )
 else : send_string( connect, 'P'+cPickle.dumps( data ) )
 #if LOG[0] == 3 : pprint.pprint( data, LOG[1] ); LOG[1].flush()
 #--------------------------------------------------------
def recv( connect ) :
 string = recv_string( connect )
 if string.startswith('S') : data = string[1:]
 elif string.startswith('P') : data = cPickle.loads( string[1:] )
 #if LOG[0] == 3 : pprint.pprint( data, LOG[1] ); LOG[1].flush()
 return data