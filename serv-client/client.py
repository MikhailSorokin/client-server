#!/usr/bin/python -u
from random import randint, random, uniform
from operator import add
from mysocket1 import *
from scipy import integrate
import numpy as np
import matplotlib.pyplot as plt
from ParetoDomination import Pareto

#import matplotlib.pyplot as plt

#LOG[0]=3
#----------------------------------------------
s = socket(AF_INET,SOCK_STREAM)
s.connect(('127.0.0.1',1234))
#str=raw_input()
send(s,'get')
a=''
'''while True:

    if str=='exit':
        break

    send(s,str)

    a=recv(s)

    if a=='q':
        print('do you want to get another task(type "yes" or "no")?')
        str=raw_input()
        if str=='no':
            send(s,str)
            str=raw_input()


    elif a=='close':
        break
    elif a.__class__==list:

        a.sort()
        print a
        print len(a)
        str='ready'

    elif a=='free':
        str=raw_input()

s.close()
'''
a=recv(s)

def gen_population(pop_size, chrom_size, min_val, max_val):
    return [[uniform(min_val, max_val) for i in range(0, chrom_size)]
                                       for j in range(0, pop_size)]

def crossover(c1, c2):
    k = randint(0, len(c1))
    return [c1[:k] + c2[k:],
            c2[:k] + c1[k:]]
    
def mutate(c, p):
    c_new = c[:]
    if p < random():
        pos = randint(0, len(c) - 1)
        c_new[pos] += random() * 0.2 - 0.1
        return c_new
    return c

def selection(fitness):
    max_fit = max(fitness)
    p = random()
    i = randint(0, len(fitness) - 1)
    while p < fitness[i] / max_fit:
        p = random()
        i = randint(0, len(fitness) - 1)
    return i

def dummy_decode(c):
    return c

def calc_dist(res, goal):
    dist = 0
    for e, r in zip(goal, res):
        dist += (e - r) ** 2
    dist = np.sqrt(dist)
    return dist
    
def in_area(res, goal):
    dist = calc_dist(res, goal)
    return dist < accur

def get_functional_time(c):
    T = []
    y0 = np.array([1, 1]) # initial conditions
    t = np.linspace(0, Tmax, n_samples)
    goal = np.array([0, 0])
    
    def func(x, t):
        u = c[5]*x[0]**2 + c[4]*x[1]**2 + c[3]*x[1]*x[0] + c[2]*x[0] + c[1]*x[1] + c[0]
        if u < -1: u = -1
        if u > 1: u = 1
        f0 = x[1]
        f1 = u

        return(f0, f1)
   
    result = integrate.odeint(func, y0, t)
    
    for i in range(len(result)):
        if not in_area(result[i], goal):
            T = t[i]
    
    dist = calc_dist(result[-1], goal)

    return T, dist

def get_fitness(c, f_decode=dummy_decode, f_fit=get_functional_time):
    decoded_c = f_decode(c)
    return f_fit(decoded_c)

def evolve(pop, mutation_rate=0.4):
    new_pop = []
    functionals = []
    
    for c in pop:
        functionals.append(get_fitness(c))
    
    p = Pareto()
    ranks = p.assign_ranks(functionals)
    fitness = p.set_fitness_from_ranks(ranks, len(pop))
    
    #print min(fitness)
    
    while len(new_pop) <= len(pop):
        while True:
            f1, f2 = selection(fitness), selection(fitness)
            if (f1 != f2):
                children = crossover(pop[f1], pop[f2])
                new_pop.extend(children)
                break
    
    new_pop.extend(pop)
    
    functionals = []
    for c in new_pop:
        functionals.append(get_fitness(c))
    
    ranks = p.assign_ranks(functionals)
    fitness = p.set_fitness_from_ranks(ranks, len(new_pop))
    
    fp = zip(fitness, new_pop)
    fp.sort(key=lambda x: x[0])
    fp = fp[int(len(fp)/2):]
    fitness, new_pop = zip(*fp)
    fit=functionals[fitness.index(max(fitness))]
    print fit
    time.append(fit[0])
    eps.append(fit[1])
    return new_pop

if __name__ == '__main__':

    accur = 0.1
    Tmax = 20
    n_samples = 100
    time=[]
    eps=[]
    U = gen_population(30, 6, -5, 5)
    
    for i in range(0, 50):
        U = evolve(U)
    print time
    print eps
    plt.plot(time,eps)
    plt.show()

