# -*- coding: utf-8 -*-
"""
Created on Sun Apr 17 13:17:39 2011

@author: David
"""

class Pareto:
    
    def dominate(self, F1, F2):
        d = False
        for i in range(len(F1[1])):
            if F1[1][i] > F2[1][i]:
                d = True
            elif F2[1][i] > F1[1][i]:
                return False
        return d
    
    def non_dominated_front(self, Group):
        Front = []
        for G in Group:
            Front.append(G)
            tmpFront = Front[:]
            for C in Front:
                if G != C:
                    if self.dominate(G, C):
                        tmpFront.remove(G)
                        break
                    elif self.dominate(C, G):
                        tmpFront.remove(C)
            Front = tmpFront[:]
        return Front
    
    def assign_ranks(self, functionals):
        tmp_functionals = [(i, functionals[i]) for i in range(len(functionals))]
        ranks = []
        i = 0
        while tmp_functionals != []:
            ranks.append(self.non_dominated_front(tmp_functionals))
            for R in ranks[i]:
                tmp_functionals.remove(R)
            i += 1
        
        return ranks

    def set_fitness_from_ranks(self, ranks, pop_size):
        Fitness = [0.0 for i in range(pop_size)]
        cr = 0.0
        for R in ranks:
            for e in R:
                Fitness[e[0]] = 1.0 / (1.0 + cr)
            cr += 1
        return Fitness